#define	F_COMMON
#define	F_GRAFIX
//#define	WINMOVE_Included
#include	"dosdefs.h"
#include	<stdio.h>
#ifdef WIN32
#include	<io.h>
#endif
#include	<stdarg.h>
#include	<stdlib.h>
#include	<string.h>
#include	"modfile.h"
#include	"myangles.h"
#include	"worldinc.h"
#include	"animdata.h"									//RJS 10Feb97

#include	"uniqueid.h"										//ARM 09Aug96
#include	"world.h"
#include	"fileman.h"
#include	"makebf.h"
#define		TEXTREF_NOTYPES
#include	"textref.h"
#include	"persons2.h"
#include	<string.h>

#include	"myerror.h"
#include	"shapes.h"
#include	"mymath.h"
#include	"flymodel.h"									//PD 01Nov95
#include	"missman2.h"
//MATHABLE	(ANGLES);

#include	"bitfield.h"
#include	"3dcom.h"
#include	"mytime.h"
#include	"flymodel.h"

#include	"ai.h"

#include	"viewsel.h"											//PD 26Jun96

#include	"landscap.h"

#include	"savegame.h"
#include	"aaa.h"
#include	"shpinstr.h"										//RJS 01Dec97
#include	"stub3d.h"
//TempCode ARM 12Sep96 #include 	<windows.h>											//ARM 09Aug96
//TempCode ARM 12Sep96 #include 	<windowsx.h>										//ARM 09Aug96
//TempCode ARM 12Sep96 #include 	<mmsystem.h>										//ARM 09Aug96
//TempCode ARM 12Sep96 #include	"c:\dxsdk\sdk\inc\dplay.h"							//ARM 09Aug96
#include	"winmove.h"											//ARM 09Aug96

#include	"modinst.h"
//#include	"replay.h"
